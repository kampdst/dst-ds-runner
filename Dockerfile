FROM ubuntu:20.04

ARG PUID=1000

# options: Master | Caves
ENV SHARD Master 
ENV CLUSTER kdst

# RUN dpkg --add-architecture i386
RUN apt update
RUN apt upgrade -y
RUN apt install -y wget lib32stdc++6 lib32gcc1 libcurl4-gnutls-dev

RUN useradd -u $PUID -m steam
# RUN mkdir -p /home/steam/steamcmd
# RUN wget -qO- https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz | tar xvz -C /home/steam/steamcmd

RUN apt purge -y wget
RUN apt clean autoclean
RUN apt clean 
RUN apt autoremove -y
RUN rm -rf /var/lib/apt/lists/*

COPY ["scripts/run_dst_ds.sh", "/home/steam"]
RUN chmod +x /home/steam/run_dst_ds.sh
RUN mkdir -p /home/steam/.klei/DoNotStarveTogether
RUN mkdir -p /home/steam/app
RUN chown steam:steam -R /home/steam/

USER steam
WORKDIR /home/steam

CMD ["bash", "/home/steam/run_dst_ds.sh"]
