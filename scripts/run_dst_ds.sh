#!/bin/bash

steamcmd_dir="$HOME/steamcmd"
install_dir="$HOME/app"
cluster_name="$CLUSTER"
cluster_shard="$SHARD"
dontstarve_dir="$HOME/.klei/DoNotStarveTogether"

function fail()
{
	echo Error: "$@" >&2
	exit 1
}

function check_for_file()
{
	if [ ! -e "$1" ]; then
		fail "Missing file: $1"
	fi
}

function determine_latest_release()
{
	cd "$1"
	echo $(ls -td -- */ | head -n1 | cut -d'/' -f1)
	cd "$PWD"
}

check_for_file "$dontstarve_dir/$cluster_name/cluster.ini"
check_for_file "$dontstarve_dir/$cluster_name/cluster_token.txt"
check_for_file "$dontstarve_dir/$cluster_name/$cluster_shard/server.ini"

game_dir=$(determine_latest_release $install_dir)
check_for_file "$install_dir/$game_dir/bin"

cd "$install_dir/$game_dir/bin64" || fail

./dontstarve_dedicated_server_nullrenderer_x64 \
	-console \
	-cluster "$cluster_name" \
	-shard "$cluster_shard" \
	-monitor_parent_process $$ 