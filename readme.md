Don't Starve Together - Dedicated Server Runner
===============================================

_Work in Progress_!

This project runs a dedicated [Don't Starve Together](https://store.steampowered.com/app/322330/Dont_Starve_Together/) server provided the game data and configuration files are shared via a data volume.


ToDo
----
2. Does it work?
3. Consider removing Master/Caves naming.


Usage
-----

``` bash
# build docker image
docker build -t kampdst/dst-ds-runner:dev .

# VOLUMES
# `app-data` from kampdst/steamcmd-stager
# `app-config` for server configration
# CONFIG in `app-config`
# provide `cluster.ini`
# provide `cluster_token.txt`
# provide `Master$/server.ini` or `Caves/server.ini`
docker run -it \
	-v app-data:/home/steam/app \
	-v app-config:/home/steam/.klei/DoNotStarveTogether/ \
	kampdst/dst-ds-runner:dev
```

References
----------

I started this project over four years ago in varous ways. I didn't document well where I got various aspects to glue together. To the best of my knowledge, the loading script was based off of [this blog](https://tianqing370687.github.io/2017/07/15/%E6%B8%B8%E6%88%8F-Linux-%E4%B8%8A%E6%90%AD%E5%BB%BA%E9%A5%A5%E8%8D%92%E8%81%94%E6%9C%BA%E7%89%88-Don-t-Starve-Together%E6%9C%8D%E5%8A%A1%E5%99%A8/).


Licenses
--------

See [LICENSE](./LICENSE) for details on this project's licensing.

